import os
from pixdisplay import PixDisplay
import random
import glob
from clock import Clock

def getRandDir(path):
	return random.choice(glob.glob(path+'/*/'))

if __name__ == "__main__":
	old_display = None
	old_display = None
	clock=Clock()
	i=0
	#print("--------------- START ---------------")
	current_display = PixDisplay(getRandDir('./sprites'), 1,1000, 0.2,1)
	while True:
		try:
			#print("--------------- LOOP ---------------")
			if i==30:i=0
			if i==0:
				current_display = PixDisplay(getRandDir('./sprites'), 1,2000, 0.2,1)
				clock.setPalette()
				#clock.changeMode()
			else:
				current_display = PixDisplay("", 1,300, 0.2,1,clock=clock)
			if old_display != None:
				last_frame = old_display._sprite.pop()._pixels
				#current_display.process(last_frame, random.randint(0, 1))
				current_display.process(last_frame, 0 if i>1 else 1)
			else:
				current_display.process()
			old_display = current_display
			i+=1
		except:
			current_display.blank()
			quit()
