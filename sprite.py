from PIL import Image

class Sprite:
    def __init__(self, path,rotate=0):
        self._path=path
        self._pixels=[]
        self.type=path.split('-')[-1].split('.')[1]
        self._duration=int(path.split('-')[-1].split('.')[0])
        fileImg=Image.open(path)
        if rotate != 0:
            fileImg=fileImg.rotate(rotate)
        fileImg = fileImg.convert('RGB')
        largeur , hauteur=fileImg.size
        for y in range (hauteur):
            for x in range (largeur):
                self._pixels.append(fileImg.getpixel((x,y)))