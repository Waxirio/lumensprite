import datetime
import random
from PIL import Image

class Clock:

    def __init__(self):
        self.mode=0 # 0 digital 1 analog
        self._heure=0
        self._minute=0
        self._seconde=0
        self._colorbackground=(0,0,0)
        self._colorHeureB=(0,0,0)
        self._colorMinuteB=(0,0,0)
        self._colorSecondeB=(0,0,0)
        self._colorheure=(0,100,255)
        self._colorminute=(232,232,0)
        self._colorseconde1=(13,235,13)
        self._colorseconde2=(200,0,77)
        self._pixels=[]
        self._indexH=[20,21,29,37,45,44,43,42,34,26,18,19]
        self._indexM=[12,13,14,22,30,38,46,54,53,52,51,50,49,41,33,25,17,9,10,11]
        self._indexM12=[27,28]
        self._indexS=[4,5,6,7,15,23,31,39,47,55,63,62,61,60,59,58,57,56,48,40,32,24,16,8,0,1,2,3,35,36]
        self._duration = 300
        for i in range(64):
            self._pixels.append(self._colorbackground)

    def setPalette(self):
        nColMax=4
        nLigMax=10
        fileImg=Image.open("palette.png")
        fileImg = fileImg.convert('RGB')
        nLigne = random.randint(0,nLigMax)
        nColonne = random.randint(0,nColMax)
        nColonne2=nColonne
        while (nColonne == nColonne2):
            nColonne2 = random.randint(0,nColMax)
        self._colorheure=fileImg.getpixel((nColonne,nLigne))
        self._colorheureB=fileImg.getpixel((nColonne2,nLigne))
        nColonne3 = random.randint(0,nColMax)
        while (nColonne3 == nColonne):
            nColonne3=random.randint(0,nColMax)
        while (nColonne3 == nColonne2):
            nColonne2 = random.randint(0,nColMax)
        self._colorminute=fileImg.getpixel((nColonne3,nLigne))
        self._colorMinuteB=fileImg.getpixel((nColonne2,nLigne))
        nColonne = random.randint(0,nColMax)
        nColonne2=nColonne
        while (nColonne == nColonne2):
            nColonne2 = random.randint(0,nColMax)
        self._colorseconde1=fileImg.getpixel((nColonne,nLigne))
        self._colorSeconde2=fileImg.getpixel((nColonne2,nLigne))
        self._colorSecondeB=fileImg.getpixel((nColonne2,nLigne))



    def GetPixels(self):
        if (self.mode == 0):
          self.GetPixelsDigital()
        else:
          self.GetPixelsAnalog()
          
          
    def changeMode(self):
      if (self.mode == 0):
        self.mode = 1
      else:
        self.mode=0

    def GetPixelsAnalog(self):
        now=datetime.datetime.now()
        self._heure=now.hour%12
        self._minute=now.minute
        self._seconde=now.second
        i=1
        # les heures
        for index in self._indexH:
            if self._heure >= i:
                self._pixels[index]=self._colorheure
            else:
                self._pixels[index]=self._colorHeureB
            i+=1

        # les minutes
        i=0
        # un carre autour = 3 imnutes
        for index in self._indexM:
            if int(self._minute/3) > i:
                self._pixels[index]=self._colorminute
            else:
                self._pixels[index]=self._colorMinuteB
            i+=1
        #si on a un nombre de minutes non divisible par 3 on ajout un ou deux carre au centre
        if (self._minute%3) >= 1:
            self._pixels[self._indexM12[0]]=self._colorminute
        else:
            self._pixels[self._indexM12[0]]=self._colorMinuteB

        if (self._minute%3) == 2:
            self._pixels[self._indexM12[1]]=self._colorminute
        else:
            self._pixels[self._indexM12[1]]=self._colorMinuteB

        # les secondes
        colorS = self._colorseconde1
        colorB = self._colorSecondeB
        seconde = self._seconde
        if self._seconde > 30:
            colorS = self._colorseconde2
            colorB = self._colorseconde1
            seconde = self._seconde-30
        i=1
        for index in self._indexS:
            if seconde >= i:
                self._pixels[index]=colorS
            else:
                self._pixels[index]=colorB
            i+=1

    def GetPixelsDigital(self):
        now=datetime.datetime.now()
        self._heure=now.hour
        self._minute=now.minute
        self._seconde=now.second
        #tab1=[2,10,18,26,34]
        #tab2=[0,1,2,10,16,17,18,24,32,33,34]
        #tab3=[0,1,2,10,16,17,18,26,32,33,34]
        #tab4=[0,2,8,10,16,17,18,26,34]
        #tab5=[0,1,2,8,16,17,18,26,32,33,34]
        #tab6=[0,1,2,8,16,17,18,24,26,32,33,34]
        #tab7=[0,1,2,10,18,26,34]
        #tab8=[0,1,2,8,10,16,17,18,24,26,32,33,34]
        #tab9=[0,1,2,8,10,16,17,18,26,32,33,34]
        #tab0=[0,1,2,8,10,16,18,24,26,32,323,34]
        tabPoint = [56]
        pos=[0,4,25,29]
        tabchiffre=[[0,1,2,8,10,16,18,24,26,32,33,34], #0
        [2,10,18,26,34],  #1
        [0,1,2,10,16,17,18,24,32,33,34], #2
        [0,1,2,10,16,17,18,26,32,33,34], #3
        [0,2,8,10,16,17,18,26,34], #4
        [0,1,2,8,16,17,18,26,32,33,34], #5
        [0,1,2,8,16,17,18,24,26,32,33,34], #6
        [0,1,2,10,18,26,34], #7
        [0,1,2,8,10,16,17,18,24,26,32,33,34], #8
        [0,1,2,8,10,16,17,18,26,32,33,34]] #9
         #background
        for i in range(64):
            self._pixels[i]=self._colorbackground
        #les chiffres
        sClock = "{:0>2d}".format(self._heure) + "{:0>2d}".format(self._minute)
        i=0
        color=(0,0,0)
        for chiffre in sClock:
            index=int(chiffre)
            j=i*4
            if i<2:
                color = self._colorheure
            else:
                color = self._colorminute
            #print("tab")
            for pix in tabchiffre[index]:
                #if self._pixels[pix+pos[i]]== self._colorbackground:
                #    self._pixels[pix+pos[i]]=color
                #else:
                #    (r,g,b)=self._pixels[pix+pos[i]]
                #    (r1,g1,b1)= color
                #    self._pixels[pix+pos[i]]=((r+r1)/2,(g+g1)/2,(b+b1)/2)
                self._pixels[pix+pos[i]]=color
            i+=1
        # les points
        color = self._colorheure
        (r,g,b) = color
        r=int((r/60)*self._seconde)
        g=int((g/60)*self._seconde)
        b=int((b/60)*self._seconde)
        color = (r,g,b)
        if self._seconde%2:
            for pix in tabPoint:
                self._pixels[pix]=color



