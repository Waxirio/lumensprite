import time
import neopixel
import board
import glob
import random
from sprite import Sprite

class PixDisplay:
  """
  PixDisplay : class
  -----
  PixDisplay object
  """

  def __init__(self, path, iteration,duration, intensity,rotate=0,clock=None):
    """
    __init__ : constructor
    -----
    self : PixDisplay object
    path : image tab to display
    iteration : number of time to display the sequence
    """
    # transition
    self._trans_root_path = "./transition/*/"
    self.rotate=rotate
    # path
    self._path=path
    # sprite
    self._sprite=[]
    # duration
    self._duration = duration
    self._anim_duration = 0
    if clock == None:
      for file in sorted(glob.glob(path+'*.png')):
        #print(file)
        sprite=Sprite(file,rotate)
        self._anim_duration+=sprite._duration
        self._sprite.append(sprite)
    else:
      self._anim_duration=duration
      clock._duration = duration
      clock.GetPixels()
      self._sprite.append(clock)
    self._iteration = iteration
    self._board = neopixel.NeoPixel(board.D18, 64, brightness=intensity, auto_write=False)

  def process(self, last_frame = None, transition = 0):
    """
    process : function
    -----
    self : PixDisplay object
    """
    # transition
    if transition != 0:
      self.transition(last_frame,self._sprite[0]._pixels)
    # launch display
    i = 0
    if self._duration != 0:
      self._iteration = int(self._duration/self._anim_duration)
    while i != self._iteration:
      for ele in self._sprite:
        self.display(ele._pixels)
        wait= int(ele._duration)/1000
        #print(wait)
        time.sleep(wait)
      i += 1

  def display(self, sprite):
    """
    process : function
    -----
    self : PixDisplay object
    sprite : Image to display
    """
    i = 0
    #print("display")
    for pix in sprite:
      self._board[i] = pix
      #self._board.setboard(i,pix)
      i += 1
    self._board.show()
    #neopixel.show()
  
  def add(self, back_frame, fore_frame):
    """
    add : function
    -----
    self : PixDisplay object
    back_frame : background frame
    fore_frame : foreground frame
    """
    final_image = []
    for i in range(0, len(back_frame)):
      # si c'est noir/vide
      if fore_frame[i] == (0, 0, 0):
        final_image.append(back_frame[i])
      else:
        final_image.append(fore_frame[i])
    return final_image

  def transition(self, last_frame,next_frame):
    """
    transition : function
    -----
    self : PixDisplay object
    last_frame : last frame displayed
    """
    # load transition
    name =  random.choice(glob.glob(self._trans_root_path))
    #print(name)
    for file in sorted(glob.glob(name + "/*.png")):
      #print("Transition")
      # on construit la nouvelle image
      ele = Sprite(file)
      if ele.type == 'N':
        frame=next_frame
      else:
        frame=last_frame
      new_sprite = self.add(frame, ele._pixels)
      # on l'affiche
      self.display(new_sprite)
      # on attend
      wait=int(ele._duration)/1000
      time.sleep(wait)
  
  def blank(self):
    for pix in range(64):
      self._board[pix] = (0,0,0)
    self._board.show()


    
