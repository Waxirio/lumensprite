#util pour changer en masse la vistesse des sprites
from sprite import Sprite
import os,glob

path='transition/spiral'
i=1
duration = '20'
for file in sorted(glob.glob(path + "/*.png")):
    ele = Sprite(file)
    typeele = ''
    if ele.type != 'png':
        typeele = '.'+ ele.type
    name = path + "/" +"{:0>2d}".format(i) + '-' + str(duration) + typeele + '.png' 
    print(name)
    os.rename(file, name )
    i+=1

