//import("C:/Users/matth/Downloads/WordClock-face_spanish.stl");
/////////////////////////////grille
//trou carre pour une led
ledtrou = 7;

//taille de la matrice (du circuit imprime)
cote= 68;

//epaisseur grille
ep = 5;

//trou pour les resistances
epresistance = 1.5;

//espacement (ne pas changer)
delta = (cote-8*ledtrou)/9 ;

///////////////////////////////cadre
epcadre=1;
rebord=2;
hauteur=25;
epfond = 1.5;

//cadre
module cadre(){
translate([-epcadre-rebord,-epcadre-rebord,0])difference(){
cube([cote+epcadre*2+2*rebord,cote+epcadre*2+2*rebord,hauteur]);
 translate([epcadre+rebord,epcadre+rebord,-1])  cube([cote,cote,hauteur+2]); 
 translate([epcadre,epcadre,1])  cube([cote+2*rebord,cote+2*rebord,hauteur+2]); 
    //trou pour le fils
 //translate([-1,cote-ledtrou,hauteur]) rotate([0,90,0]) cylinder(r=4,h=20,$fn=10);
 #translate([-1,cote-ledtrou-1.5,hauteur]) rotate([0,90,0]) cube([11.5+epfond,13.5,20]);
}
}



//grille
module grille(){
union()
{
    difference(){
    translate([-rebord,-rebord,0])cube([cote+rebord*2,cote+rebord*2,ep]);

        for ( i = [1 : 8] ){
            //les trous pour les resistances
            translate ([(i)*(ledtrou+delta)-ledtrou/2,-1,ep+ledtrou/2-epresistance]) rotate([-90,0,0])cylinder(r=ledtrou/2, h=cote+ 2,$fn=6);
            for ( j = [1 : 8] ){
                //les trous pour les led
                translate([i*delta+(i-1)*ledtrou,j*delta+(j-1)*ledtrou,-1])cube([ledtrou,ledtrou,ep+2]);
            }
        }
    }// fin difference

    //les 6 pins pour faire tenir le circuit imprime
    translate([ledtrou+delta+delta/2,ledtrou+delta+delta/2,ep])cylinder(r=2.5/2,h=2,$fn=20);
   
    translate([7*(ledtrou+delta)+delta/2,ledtrou+delta+delta/2,ep])cylinder(r=2.5/2,h=2,$fn=20);
    
    translate([ledtrou+delta+delta/2,7*(ledtrou+delta)+delta/2,ep])cylinder(r=2.5/2,h=2,$fn=20);
    
    translate([7*(ledtrou+delta)+delta/2,7*(ledtrou+delta)+delta/2,ep])cylinder(r=2.5/2,h=2,$fn=20);
    
    translate([(ledtrou+delta)+delta/2,4*(ledtrou+delta)+delta/2,ep])cylinder(r=2.5/2,h=2,$fn=20);
    
    translate([7*(ledtrou+delta)+delta/2,4*(ledtrou+delta)+delta/2,ep])cylinder(r=2.5/2,h=2,$fn=20);
    
} // fin grille
}

// fermeture boite
//0 = boite ferme

module fermeture(){
transHaut=0;
translate([-epcadre-rebord,-epcadre-rebord,hauteur+transHaut])
union(){

cube([cote+epcadre*2+2*rebord,cote+epcadre*2+2*rebord,2]);
translate([rebord/2,rebord/2,-epfond])cube([cote+rebord*2,cote+rebord*2,1.5]);
color("red")translate([rebord/2+64,rebord/2+10,-5]) rotate([0,0,90]) piattache();
}
}

module pied(){
 difference(){
 cylinder(r=3/2,h=4,$fn=20);
 translate ([0,0,-1])cylinder(r=0.5,h=6,$fn=20);
}   
}

module piattache(){
 pied();
 translate ([23,58,0])  pied();
 translate ([23,0,0])   pied();
 translate ([0,58,0])   pied();
 translate ([50,23,0])   pied();
 translate ([50,23-21,0])   pied();
}

cadre();
color("red") grille();
fermeture();
piattache();

//translate ([9,7.5,6]) rotate([-90,00,0])cylinder(r=1,h=53);




