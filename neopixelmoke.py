import pygame
import math


class NeoPixel():

    _board=[]

    def __init__(self, pin, nb , brightness, auto_write):
        self.pixel=80
        self.espace=6
        self.size=8*self.pixel+9*self.espace
        self.screen= pygame.display.set_mode((self.size, self.size))
        self.game_surf = pygame.surface.Surface((self.size, self.size))
        self._board=[]
        pygame.draw.rect(self.game_surf,pygame.Color(255,255,255),(0,0,self.size,self.size))
        for i in range(nb):
            self._board.append((0,0,0))
        #return self._board

    def show(self):
        i=0
        pygame.draw.rect(self.game_surf,pygame.Color(255,255,255),(0,0,self.size,self.size))
        for point in self._board:
            x=(i%8)
            y=math.floor(i/8)
            XT = self.espace*(x+1)+self.pixel*(x)
            YT = self.espace*(y+1)+self.pixel*(y)
            # print('XT:'+str(XT))
            # print('YT:'+str(YT))
            pygame.draw.rect(self.game_surf,pygame.Color(point),(XT,YT,self.pixel,self.pixel))
            i+=1
        self.screen.blit(self.game_surf,(0,0))
        pygame.display.update()
        for event in pygame.event.get():
                #pour quitter le jeux
                if event.type == pygame.QUIT:
                    menu= False
                    quit()

    def setboard(self, index, color):
        self._board[index]=color

