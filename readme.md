# Affichage des sprites 8x8 en led rgb

-   [Matériel](#Matériel)

-   [Install](#install)
    -   [Requirements](#requirements)
    -   [Installation](#installation)

-   [Boitier](#Boitier)



# Matériel
 
 - [Raspberry pi zero W](https://www.kubii.fr/les-cartes-raspberry-pi/1851-raspberry-pi-zero-w-kubii-3272496006997.html)  

 - [Voltage converter (rasp GPIO 3.3V - led data 5V)](https://www.amazon.fr/gp/product/B07ZDK6DKY/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1)  

 - [matrice led rgb 8x8](https://fr.banggood.com/CJMCU-64-Bit-WS2812-5050-RGB-LED-Driver-Development-Board-p-981678.html?rmmds=myorder&cur_warehouse=UK)

 - [imprimante 3D](https://ultimaker.com/3d-printers/ultimaker-original-plus)

# Install

## Requirements

-   [pi os light](https://raspberry-pi.fr/telechargements/)

-   [python3](https://www.python.org/downloads/)

-   [lib python neopixels](https://learn.adafruit.com/neopixels-on-raspberry-pi/python-usage)

-   [lib python pillow](https://pypi.org/project/Pillow/) (installer en sudo pour root)

-   [lib python pygame](https://www.pygame.org/news) (juste pour simuler l'afficheur)


## Installation

Cloner le projet:

```shell
git clone git@gitlab.com:Waxirio/lumensprite.git <path>
cd <path>
```

lancer le projet

```shell
sudo python3 main.py
```

## Boitier

![fermée](https://gitlab.com/Waxirio/lumensprite/-/raw/master/boite3D/boite1.JPG) 

![eclaté en 3 pièces](https://gitlab.com/Waxirio/lumensprite/-/raw/master/boite3D/boite2.JPG) 

![apercu](https://gitlab.com/Waxirio/lumensprite/-/raw/master/boite3D/boiteDebug.gif) 

![print3D](https://gitlab.com/Waxirio/lumensprite/-/raw/master/boite3D/boitier.PNG) 

![electronique](https://gitlab.com/Waxirio/lumensprite/-/raw/master/boite3D/misenplace.PNG)

![final](https://gitlab.com/Waxirio/lumensprite/-/raw/master/boite3D/clock.gif) 